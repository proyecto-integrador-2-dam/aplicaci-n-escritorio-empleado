/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicRuta;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author batoi
 */
public class CamionDAO implements GenericDAO<BatoiLogicRuta>{

    private final Session session;

    public CamionDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }
    
    @Override
    public BatoiLogicRuta findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicRuta> findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicCamion a").list();
    }

    @Override
    public List<BatoiLogicRuta> findByExample(BatoiLogicRuta t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicRuta> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(BatoiLogicRuta t) throws Exception {
        BatoiLogicRuta alInsertar = (BatoiLogicRuta) t;
        session.beginTransaction();
        session.save(alInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(BatoiLogicRuta t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

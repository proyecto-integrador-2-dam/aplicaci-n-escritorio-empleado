/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicAlbaran;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class AlbaranDAO implements GenericDAO<BatoiLogicAlbaran>{
    
    private final Session session;
    
    public AlbaranDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicAlbaran findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicAlbaran> findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicAlbaran a").list();
    }

    @Override
    public List<BatoiLogicAlbaran> findByExample(BatoiLogicAlbaran t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicAlbaran> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(BatoiLogicAlbaran t) throws Exception {
        BatoiLogicAlbaran alInsertar = (BatoiLogicAlbaran) t;
        session.beginTransaction();
        session.save(alInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(BatoiLogicAlbaran t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

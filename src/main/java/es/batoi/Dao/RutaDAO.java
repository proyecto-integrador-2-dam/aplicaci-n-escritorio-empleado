/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicRepartidor;
import es.batoi.modelos.BatoiLogicRuta;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class RutaDAO implements GenericDAO<BatoiLogicRuta>{

    private final Session session;
    
    public RutaDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }
    
    @Override
    public BatoiLogicRuta findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicRuta> findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicRuta a").list();
    }

    public List<BatoiLogicRuta> findByRepartidor(BatoiLogicRepartidor repartidor) throws Exception {
        return session.createQuery("select a from BatoiLogicRuta a where a.batoiLogicRepartidor.id = " + repartidor.getId()).list();
    }

    @Override
    public List<BatoiLogicRuta> findByExample(BatoiLogicRuta t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicRuta> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(BatoiLogicRuta t) throws Exception {
        BatoiLogicRuta alInsertar = (BatoiLogicRuta) t;
        session.beginTransaction();
        session.save(alInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(BatoiLogicRuta t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicPedido;
import es.batoi.modelos.BatoiLogicProductoproveedor;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author batoi
 */
public class ProductoProveedorDAO implements GenericDAO {
    private final Session sesion;

    public ProductoProveedorDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicProductoproveedor findByPK(int id) throws Exception {
        return (BatoiLogicProductoproveedor) sesion.get(BatoiLogicProductoproveedor.class, id);
    }

    @Override
    public List<BatoiLogicProductoproveedor> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicProductoproveedor a").list();
    }

    public List<BatoiLogicProductoproveedor> findByProducto(int id) throws Exception {
        return sesion.createQuery("select a from BatoiLogicProductoproveedor a where a.batoiLogicProducto.id = " + id).list();
    }

    @Override
    public List<BatoiLogicProductoproveedor> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicProductoproveedor cliInsertar = (BatoiLogicProductoproveedor) t;
        sesion.beginTransaction();
        sesion.save(cliInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicProductoproveedor cliBorrar = findByPK(id);
        delete(cliBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicProductoproveedor cliBorrar = (BatoiLogicProductoproveedor) t;
        sesion.getTransaction().begin();
        sesion.delete(cliBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception  {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicProductoproveedor").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}

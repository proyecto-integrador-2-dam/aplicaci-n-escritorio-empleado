package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.*;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author Javier Soler Artero
 */
public class LinPedidoDAO implements GenericDAO {
    
    private final Session session;
    
    public LinPedidoDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public Object findByPK(int id) throws Exception {
        return (BatoiLogicLinpedido) session.get(BatoiLogicLinpedido.class, id);
    }

    @Override
    public List findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicLinpedido a").list();
    }

    @Override
    public List findByExample(Object t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List findByPedido(BatoiLogicPedido t) throws Exception {
        return session.createQuery("select a from BatoiLogicLinpedido a where a.batoiLogicPedidoByPedido.id = " + t.getId()).list();
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicLinpedido repInsertar = (BatoiLogicLinpedido) t;
        session.beginTransaction();
        session.save(repInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicLinpedido update = (BatoiLogicLinpedido) t;
        BatoiLogicLinpedido find = (BatoiLogicLinpedido) findByPK(update.getId());
        
        session.getTransaction().begin();
        session.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicLinpedido repBorrar = (BatoiLogicLinpedido) findByPK(id);
        delete(repBorrar);
        return true;
    }
    
    public boolean delete(Object t) throws Exception {
        BatoiLogicLinpedido repBorrar = (BatoiLogicLinpedido) t;
        session.getTransaction().begin();
        session.delete(repBorrar);
        session.getTransaction().commit();
        return true;
    }
    
    
    
}

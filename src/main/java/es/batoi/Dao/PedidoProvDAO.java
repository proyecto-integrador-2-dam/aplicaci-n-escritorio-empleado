package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicAlbaran;
import es.batoi.modelos.BatoiLogicPedidoprov;
import es.batoi.modelos.BatoiLogicRuta;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author Javier Soler Artero
 */
public class PedidoProvDAO implements GenericDAO {
    
    private final Session session;
    
    public PedidoProvDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public Object findByPK(int id) throws Exception {
        return (BatoiLogicPedidoprov) session.get(BatoiLogicPedidoprov.class, id);
    }

    @Override
    public List findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicPedidoprov a").list();
    }
    
    public List findPrepared() throws Exception {
        return session.createQuery("select a from BatoiLogicPedidoprov a where a.estado = 'EN PREPARACION'").list();
    }

    @Override
    public List findByExample(Object t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicPedidoprov repInsertar = (BatoiLogicPedidoprov) t;
        session.beginTransaction();
        session.save(repInsertar);
        session.getTransaction().commit();
        return true;
    }
    
    public boolean insertRuta(Object t) throws Exception {
        BatoiLogicRuta alInsertar = (BatoiLogicRuta) t;
        session.beginTransaction();
        session.save(alInsertar);
        session.getTransaction().commit();
        return true;
    }
    
    public boolean insertAlbaran(Object t) throws Exception {
        BatoiLogicAlbaran alInsertar = (BatoiLogicAlbaran) t;
        session.beginTransaction();
        session.save(alInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicPedidoprov update = (BatoiLogicPedidoprov) t;
        BatoiLogicPedidoprov find = (BatoiLogicPedidoprov) findByPK(update.getId());
        
        session.getTransaction().begin();
        session.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicPedidoprov repBorrar = (BatoiLogicPedidoprov) findByPK(id);
        delete(repBorrar);
        return true;
    }
    
    public boolean delete(Object t) throws Exception {
        BatoiLogicPedidoprov repBorrar = (BatoiLogicPedidoprov) t;
        session.getTransaction().begin();
        session.delete(repBorrar);
        session.getTransaction().commit();
        return true;
    }

    public boolean stockSuficiente(BatoiLogicPedidoprov pedido) {
        boolean suficiente = true;
        session.getTransaction().begin();
        
        session.getTransaction().commit();
        
        return suficiente;
    }
    
}

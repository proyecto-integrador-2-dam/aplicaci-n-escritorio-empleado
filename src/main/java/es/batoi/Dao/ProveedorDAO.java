/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicProveedor;
import org.hibernate.Session;

import java.util.List;

/**
 *
 * @author batoi
 */
public class ProveedorDAO implements GenericDAO{
    private final Session sesion;

    public ProveedorDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicProveedor findByPK(int id) throws Exception {
        return (BatoiLogicProveedor) sesion.get(BatoiLogicProveedor.class, id);
    }

    @Override
    public List<BatoiLogicProveedor> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicProveedor a").list();
    }

    @Override
    public List<BatoiLogicProveedor> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicProveedor repInsertar = (BatoiLogicProveedor) t;
        sesion.beginTransaction();
        sesion.save(repInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicProveedor repBorrar = findByPK(id);
        delete(repBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicProveedor repBorrar = (BatoiLogicProveedor) t;
        sesion.getTransaction().begin();
        sesion.delete(repBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception  {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicProveedor").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}

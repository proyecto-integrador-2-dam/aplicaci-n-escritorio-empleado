/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicUbicacion;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class UbicacionDAO implements GenericDAO<BatoiLogicUbicacion>{

    private final Session sesion;

    public UbicacionDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }
    
    @Override
    public BatoiLogicUbicacion findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicUbicacion> findAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicUbicacion> findByExample(BatoiLogicUbicacion t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BatoiLogicUbicacion> findBySQL(String sqlselect) throws Exception {
        return sesion.createQuery(sqlselect).list();
    }

    @Override
    public boolean insert(BatoiLogicUbicacion t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(BatoiLogicUbicacion t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

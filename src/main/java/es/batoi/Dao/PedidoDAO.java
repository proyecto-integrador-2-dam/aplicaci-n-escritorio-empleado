package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicAlbaran;
import es.batoi.modelos.BatoiLogicFactura;
import es.batoi.modelos.BatoiLogicPedido;
import es.batoi.modelos.BatoiLogicRepartidor;
import es.batoi.modelos.BatoiLogicRuta;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Javier Soler Artero
 */
public class PedidoDAO implements GenericDAO {
    
    private final Session session;
    
    public PedidoDAO() {
        session = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public Object findByPK(int id) throws Exception {
        return (BatoiLogicPedido) session.get(BatoiLogicPedido.class, id);
    }

    @Override
    public List findAll() throws Exception {
        return session.createQuery("select a from BatoiLogicPedido a").list();
    }
    
    public List findPrepared() throws Exception {
        return session.createQuery("select a from BatoiLogicPedido a where a.estado = 'EN PREPARACION'").list();
    }

    @Override
    public List findByExample(Object t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicPedido repInsertar = (BatoiLogicPedido) t;
        session.beginTransaction();
        session.save(repInsertar);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        BatoiLogicPedido update = (BatoiLogicPedido) t;
        BatoiLogicPedido find = (BatoiLogicPedido) findByPK(update.getId());
        
        session.getTransaction().begin();
        find.setEstado("EN RUTA");
        session.getTransaction().commit();
        
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicPedido repBorrar = (BatoiLogicPedido) findByPK(id);
        delete(repBorrar);
        return true;
    }
    
    public boolean delete(Object t) throws Exception {
        BatoiLogicPedido repBorrar = (BatoiLogicPedido) t;
        session.getTransaction().begin();
        session.delete(repBorrar);
        session.getTransaction().commit();
        return true;
    }
    
    
    
}

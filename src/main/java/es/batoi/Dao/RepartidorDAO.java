/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.Dao;

import es.batoi.Connection.HibernateConnection;
import es.batoi.modelos.BatoiLogicRepartidor;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class RepartidorDAO implements GenericDAO{
    private final Session sesion;

    public RepartidorDAO() {
        sesion = HibernateConnection.getSessionFactory().openSession();
    }

    @Override
    public BatoiLogicRepartidor findByPK(int id) {
        return (BatoiLogicRepartidor) sesion.get(BatoiLogicRepartidor.class, id);
    }

    @Override
    public List<BatoiLogicRepartidor> findAll() throws Exception {
        return sesion.createQuery("select a from BatoiLogicRepartidor a").list();
    }

    @Override
    public List<BatoiLogicRepartidor> findByExample(Object muestra) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        System.out.println("No implementado");
        return null;
    }

    @Override
    public boolean insert(Object t) throws Exception {
        BatoiLogicRepartidor repInsertar = (BatoiLogicRepartidor) t;
        sesion.beginTransaction();
        sesion.save(repInsertar);
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        // Articulo artActualizar = (Articulo) t;
        sesion.getTransaction().begin();
        // sesion.update(grupoActualizar);   // no necesario
        sesion.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        BatoiLogicRepartidor repBorrar = findByPK(id);
        delete(repBorrar);
        return true;
    }

    public boolean delete(Object t) throws Exception {
        BatoiLogicRepartidor repBorrar = (BatoiLogicRepartidor) t;
        sesion.getTransaction().begin();
        sesion.delete(repBorrar);
        sesion.getTransaction().commit();
        return true;
    }

    // Ejemplo consulta: obtener sólo los nombres de los Clientes
    public List<String> findAllName() throws Exception  {
        List<String> registros = sesion.createQuery("select nombre from BatoiLogicRepartidor").list();
        return registros;
    }

    public void cerrar() {
        sesion.close();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.batoi.controlador;

import es.batoi.App;
import es.batoi.Dao.RepartidorDAO;
import es.batoi.modelos.BatoiLogicRepartidor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author batoi
 */
public class LoginController implements Initializable {

    @FXML
    private TextField tfUsuario;

    @FXML
    private TextField tfContrasenya;

    RepartidorDAO repartidorDAO;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            repartidorDAO = new RepartidorDAO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void iniciarSesion(ActionEvent event) throws IOException {
        try {

            List<BatoiLogicRepartidor> repartidores = repartidorDAO.findAll();

            boolean inicioValido = false;
            BatoiLogicRepartidor repartidorActual = new BatoiLogicRepartidor();
            for (BatoiLogicRepartidor repartidor : repartidores) {
                //Buscamos un repartidor con las mismas credenciales.
                if (repartidor.getId() == Integer.parseInt(tfUsuario.getText()) && repartidor.getPassw().equals(tfContrasenya.getText())) {
                    inicioValido = true;
                    repartidorActual = repartidor;
                }
            }

            if (inicioValido) {
                // Le pasamos al principal el usuario logueado.
                PrincipalController.repartidorLogueado(repartidorActual);

                Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();

                primaryStage.setScene(new Scene(loadFXML("ViewPrincipal")));
                primaryStage.setTitle("BatoiLOGIC");
                primaryStage.show();

                primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        // Cerramos la ventana.
                        primaryStage.close();
                        repartidorDAO.cerrar();
                        // Reiniciamos el programa (para volver a mostrar el inicio de sesión).
                        Platform.runLater(() -> {
                            try {
                                new App().start(new Stage());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                });
            } else {
                pantallaErrores(new Exception("El ID o la contraseña son incorrectos. Inténtelo de nuevo."));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Función que mostrará por pantalla los mensajes de error.
     *
     * @param ex
     */
    private void pantallaErrores(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Se ha producido un error");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("El trazado ha devuelto el siguiente error:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}

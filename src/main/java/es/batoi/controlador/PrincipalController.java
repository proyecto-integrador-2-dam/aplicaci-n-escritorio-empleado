package es.batoi.controlador;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import es.batoi.App;
import es.batoi.Dao.*;
import es.batoi.modelos.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author batoi
 */
public class PrincipalController implements Initializable, MapComponentInitializedListener {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private AnchorPane apPedidosActivos;
    @FXML
    private AnchorPane apAlbaranes;
    @FXML
    private AnchorPane apCamiones;
    @FXML
    private TableColumn<BatoiLogicPedido, Integer> idPedido;
    @FXML
    private TableColumn<BatoiLogicPedido, Double> precio;
    @FXML
    private TableColumn<BatoiLogicPedido, String> historialfecha;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcObservaciones;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcDireccion;
    @FXML
    private TableColumn<?, ?> cantProduct;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcCliente;
    @FXML
    private TextField tfCliente;
    @FXML
    private TextField tfPedido;
    @FXML
    private DatePicker dpPedidoDesde;
    @FXML
    private DatePicker dpPedidoHasta;
    @FXML
    private TableColumn<BatoiLogicPedido, Integer> tcIdPedidoAlbaran;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcEstadoAlbaran;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcObservacionesAlbaran;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcClienteAlbaran;
    @FXML
    private TableColumn<BatoiLogicPedido, String> tcDireccionAlbaran;

    @FXML
    private TableView<BatoiLogicRepartidor> tvRepartidores;
    @FXML
    private TableColumn<BatoiLogicRepartidor, String> tcNombreRepartidor;
    @FXML
    private TableColumn<BatoiLogicRepartidor, String> tcApellidosRepartidor;
    @FXML
    private TableColumn<BatoiLogicRepartidor, String> tcCamionRepartidor;

    @FXML
    private TableView<BatoiLogicPedido> tvAlbaranes;
    @FXML
    private TableView<BatoiLogicPedido> tvPedidosActivos;
    @FXML
    private GoogleMapView mapView;
    private GoogleMap map;

    @FXML
    private Label lbUsuario;

    private List<BatoiLogicProducto> productosDisponibles = new ArrayList<>();

    //Atributos de la clase
    public PedidoDAO pedidoDAO;
    public RepartidorDAO repartidorDAO;
    public ProductosDAO productosDAO;
    public ProveedorDAO proveedorDAO;
    public LinPedidoDAO linPedidoDAO;
    public AlbaranDAO albaranDAO;
    public RutaDAO rutaDAO;
    public CamionDAO camionDAO;

    private BatoiLogicRepartidor repartidorSeleccionado;
    private ObservableList<BatoiLogicPedido> listaPedidos;
    private ObservableList<BatoiLogicPedido> pedidosSeleccionados;
    private ObservableList<BatoiLogicRepartidor> listaRepartidores;
    private List<BatoiLogicProveedor> proveedores;

    private static BatoiLogicRepartidor repartidorActual;

    public static void repartidorLogueado(BatoiLogicRepartidor repartidorQueIniciaSesion) {
        repartidorActual = repartidorQueIniciaSesion;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbUsuario.setText(repartidorActual.getNombre() + " " + repartidorActual.getApellidos());

        this.pedidoDAO = new PedidoDAO();
        this.repartidorDAO = new RepartidorDAO();
        this.productosDAO = new ProductosDAO();
        this.proveedorDAO = new ProveedorDAO();
        this.albaranDAO = new AlbaranDAO();
        this.rutaDAO = new RutaDAO();
        this.camionDAO = new CamionDAO();

        //Pestaña de los camiones
        mapView.addMapInializedListener((MapComponentInitializedListener) this);

        apPedidosActivos.setVisible(true);
        apAlbaranes.setVisible(false);
        apCamiones.setVisible(false);

        //Pestaña de los pedidos activos
        this.tvPedidosActivos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.listaPedidos = FXCollections.observableArrayList();
        //Pestaña de los repartidores
        this.listaRepartidores = FXCollections.observableArrayList();
        this.tvRepartidores.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        try {
            productosDAO = new ProductosDAO();
            productosDisponibles = productosDAO.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        initializeInventario();
        initializeRealizarPedido();
        initializeProveedores();
        initializeConsultaUbica();

        //carga de datos
        try {
            mostrarPedidos();
        } catch (Exception ex) {
            pantallaErrores(new Exception("No se han podido mostrar los pedidos"));
            ex.printStackTrace();
        }
    }

    /*
    Pestaña: Seguimiento de camiones
     */
    @FXML
    private ComboBox cbRepartidor;

    private void initializeConsultaUbica() {
        try {
            List<BatoiLogicRepartidor> listRepart = repartidorDAO.findAll();
            cbRepartidor.getItems().setAll(listRepart);

        } catch (Exception ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showLocations() {
        try {
            map.clearMarkers();

            UbicacionDAO ubi = new UbicacionDAO();
            BatoiLogicRepartidor repartidor = (BatoiLogicRepartidor) cbRepartidor.getSelectionModel().getSelectedItem();
            LocalDate d = LocalDate.now();

            List<BatoiLogicUbicacion> aux = ubi.findBySQL("select a from BatoiLogicUbicacion a where a.batoiLogicRepartidor.id = " + repartidor.getId());
            List<BatoiLogicUbicacion> l = new ArrayList<>();

            for (int i = 0; i < aux.size(); i++) {
                System.out.println(aux.get(i).getFecha() + " : " + d);
                if ((aux.get(i).getFecha() + "").equals(d + "")) {
                    l.add(aux.get(i));
                }
            }

            if (l.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ubicaciones");
                alert.setHeaderText(null);
                alert.setContentText("No existen ubicaciones para el dia de hoy para ese repartidor.");

                alert.showAndWait();
            } else {
                for (int i = 0; i < l.size(); i++) {
                    BatoiLogicUbicacion ubicacion = (BatoiLogicUbicacion) l.get(i);
                    LatLong testLocation = new LatLong(ubicacion.getLatitud(), ubicacion.getLongitud());
                    MarkerOptions testMarkerOptions = new MarkerOptions();
                    testMarkerOptions.position(testLocation);

                    Date horaInsercion = ubicacion.getCreateDate();
                    SimpleDateFormat working = new SimpleDateFormat("HH:mm");
                    Marker testMarker = new Marker(testMarkerOptions);

                    map.addMarker(testMarker);

                    InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                    infoWindowOptions.content(working.format(horaInsercion));

                    InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
                    fredWilkeInfoWindow.open(map, testMarker);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que inicia el mapa
     */
    @Override
    public void mapInitialized() {
        //LatLong testLocation = new LatLong(38.690901, -0.496173);
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(38.690901, -0.496173))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(9);

        map = mapView.createMap(mapOptions);

        //Add markers to the map
        /*MarkerOptions testMarkerOptions = new MarkerOptions();
        testMarkerOptions.position(testLocation);

        Marker testMarker = new Marker(testMarkerOptions);
        map.addMarker(testMarker);*/
    }

    /*      
    Pestaña: Pedidos Activos
     */
    /**
     * Metodo para mostrar todos los pedidos
     *
     * @throws Exception
     */
    private void mostrarPedidos() throws Exception {
        ArrayList<BatoiLogicPedido> aux = (ArrayList<BatoiLogicPedido>) pedidoDAO.findPrepared();

        //Rellenar en la lista
        idPedido.setCellValueFactory(new PropertyValueFactory<>("id"));
        precio.setCellValueFactory(new PropertyValueFactory<>("total"));
        historialfecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        tcCliente.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getBatoiLogicCliente().getNombre() + " " + o.getValue().getBatoiLogicCliente().getApellidos()));
        tcObservaciones.setCellValueFactory(new PropertyValueFactory<>("observaciones"));
        tcDireccion.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getBatoiLogicDireccion().getCalle()));

        for (BatoiLogicPedido pedido : aux) {
            listaPedidos.add(pedido);
        }

        tvPedidosActivos.setItems(listaPedidos);
    }

    /**
     * Metodo para filtrar los pedidos
     *
     * @param event
     */
    @FXML
    void filtrar(ActionEvent event) {
        try {
            ObservableList<BatoiLogicPedido> listaFiltrada = FXCollections.observableArrayList();

            if (!tfCliente.getText().equals("")) {
                String nombreCliente = tfCliente.getText();

                for (BatoiLogicPedido pedido : listaPedidos) {
                    if ((pedido.getBatoiLogicCliente().getNombre() + " " + pedido.getBatoiLogicCliente().getApellidos()).contains(nombreCliente)) {
                        if (!listaFiltrada.contains(pedido)) {
                            listaFiltrada.add(pedido);
                        }
                    } else {
                        listaFiltrada.remove(pedido);
                    }
                }
            }

            if (!tfPedido.getText().equals("")) {
                int numPedido = Integer.parseInt(tfPedido.getText());

                for (BatoiLogicPedido pedido : listaPedidos) {
                    if (pedido.getId() == numPedido) {
                        if (!listaFiltrada.contains(pedido)) {
                            listaFiltrada.add(pedido);
                        }
                    } else {
                        listaFiltrada.remove(pedido);
                    }
                }
            }

            SimpleDateFormat formatoEnBD = new SimpleDateFormat("yyyy-MM-dd");
            boolean filtradoDesde = false;
            Date desde = null;

            if (dpPedidoDesde.getValue() != null) {
                filtradoDesde = true;

                desde = formatoEnBD.parse(dpPedidoDesde.getValue().toString());

                for (BatoiLogicPedido pedido : listaPedidos) {
                    if (pedido.getFecha().after(desde)) {
                        if (!listaFiltrada.contains(pedido)) {
                            listaFiltrada.add(pedido);
                        }
                    } else {
                        listaFiltrada.remove(pedido);
                    }
                }
            }

            if (dpPedidoHasta.getValue() != null) {
                Date hasta = formatoEnBD.parse(dpPedidoHasta.getValue().toString());

                for (BatoiLogicPedido pedido : listaPedidos) {

                    if (filtradoDesde) {
                        if (pedido.getFecha().after(desde) && pedido.getFecha().before(hasta)) {
                            if (!listaFiltrada.contains(pedido)) {
                                listaFiltrada.add(pedido);
                            }
                        } else {
                            listaFiltrada.remove(pedido);
                        }

                    } else {
                        if (pedido.getFecha().before(hasta)) {
                            if (!listaFiltrada.contains(pedido)) {
                                listaFiltrada.add(pedido);
                            }
                        } else {
                            listaFiltrada.remove(pedido);
                        }
                    }
                }
            }

            tvPedidosActivos.setItems(listaFiltrada);

        } catch (ParseException e) {
            pantallaErrores(e);
        }

    }

    /**
     * Metodo para quitar los filtros
     *
     * @param event
     */
    @FXML
    void limpiarFiltro(ActionEvent event) {
        listaPedidos.clear();
        tvPedidosActivos.getItems().clear();
        tfCliente.setText("");
        tfPedido.setText("");
        dpPedidoDesde.setValue(null);
        dpPedidoHasta.setValue(null);

        try {
            mostrarPedidos();
        } catch (Exception ex) {
            pantallaErrores(new Exception("No se han podido cargar los pedidos"));
        }
    }

    /*
    Cambios de pantalla
     */
    @FXML
    void volverAlbaranes(MouseEvent event) {
        apPedidosActivos.setVisible(false);
        apAlbaranes.setVisible(true);
        apCamiones.setVisible(false);
    }

    @FXML
    void volverPedidosActivos(MouseEvent event) {
        volverPedidos(event);
    }

    @FXML
    void volverPedidos(Event event) {
        try {
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            primaryStage.setScene(new Scene(loadFXML("ViewPrincipal")));
            primaryStage.setTitle("BatoiLOGIC");
            primaryStage.setResizable(false);
            primaryStage.centerOnScreen();
            primaryStage.show();
        } catch (IOException e) {
            pantallaErrores(e);
        }
    }

    /*
    Creacion de rutas y documentacion
     */
    @FXML
    void eliminarAlbaran(MouseEvent event) {
        if (pedidosSeleccionados.size() > 1) {
            BatoiLogicPedido seleccionado = tvAlbaranes.getSelectionModel().getSelectedItem();
            ObservableList<BatoiLogicPedido> nuevaLista = FXCollections.observableArrayList();

            for (BatoiLogicPedido pedido : pedidosSeleccionados) {
                if (!pedido.equals(seleccionado)) {
                    nuevaLista.add(pedido);
                }
            }

            tvAlbaranes.setItems(nuevaLista);
            pedidosSeleccionados = nuevaLista;

        } else {
            volverPedidos(event);
        }
    }

    /**
     * Metodo que pone el anchor pane principal a invisible y poner en visible
     * el fxml de creacion de albaranes
     *
     * @param event
     */
    @FXML
    void creacionAlbaranes(ActionEvent event) {

        try {
            pedidosSeleccionados = tvPedidosActivos.getSelectionModel().getSelectedItems();
            if (pedidosSeleccionados.size() > 0) {
                linPedidoDAO = new LinPedidoDAO();
                BatoiLogicLinpedido lineaPedidoSeleccionado;

                BatoiLogicPedido pedidoProblematico = new BatoiLogicPedido();
                boolean pedidoValido = true;
                for (int i = 0; i < pedidosSeleccionados.size() && pedidoValido; i++) {
                    lineaPedidoSeleccionado = (BatoiLogicLinpedido) linPedidoDAO.findByPedido(pedidosSeleccionados.get(i)).get(0);

                    if (lineaPedidoSeleccionado.getCantidad() > productosDAO.findByPK(lineaPedidoSeleccionado.getBatoiLogicPedidoByProducto().getId()).getStock()) {
                        pedidoValido = false;
                        pedidoProblematico = pedidosSeleccionados.get(i);
                    }
                }

                if (pedidoValido) {
                    apPedidosActivos.setVisible(false);
                    apAlbaranes.setVisible(true);
                    apCamiones.setVisible(false);

                    tcIdPedidoAlbaran.setCellValueFactory(new PropertyValueFactory<>("id"));
                    tcEstadoAlbaran.setCellValueFactory(new PropertyValueFactory<>("estado"));
                    tcObservacionesAlbaran.setCellValueFactory(new PropertyValueFactory<>("observaciones"));
                    tcClienteAlbaran.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getBatoiLogicCliente().getNombre() + " " + o.getValue().getBatoiLogicCliente().getApellidos()));
                    tcDireccionAlbaran.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getBatoiLogicDireccion().getCalle()));

                    tvAlbaranes.setItems(pedidosSeleccionados);

                } else {
                    throw new Exception("Falta stock para sevir alguno de los pedidos seleccionados. Pida stock a algún proveedor para poder servir esta entrega -> " + pedidoProblematico.getId());
                }

            } else {
                throw new Exception("Seleccione algún pedido para generar albaranes.");
            }

        } catch (Exception e) {
            pantallaErrores(e);
        }
    }

    /**
     * Metodo que pone el anchor pane de los albaranes a invisible y poner en
     * visible el fxml de la seleccion de los camiones
     */
    @FXML
    void mostrarCamiones() {
        apPedidosActivos.setVisible(false);
        apAlbaranes.setVisible(false);
        apCamiones.setVisible(true);

        cargarCamiones();
    }

    private void cargarCamiones() {
        try {
            ArrayList<BatoiLogicRepartidor> listaAux = (ArrayList<BatoiLogicRepartidor>) repartidorDAO.findAll();

            if (listaRepartidores.size() == 0) {
                for (BatoiLogicRepartidor repartidor : listaAux) {
                    listaRepartidores.add(repartidor);
                }
            }

            tcNombreRepartidor.setCellValueFactory(new PropertyValueFactory<>("nombre"));
            tcApellidosRepartidor.setCellValueFactory(new PropertyValueFactory<>("apellidos"));
            tcCamionRepartidor.setCellValueFactory(o -> new SimpleStringProperty(o.getValue().getBatoiLogicCamion().getMatricula()));

            tvRepartidores.setItems(listaRepartidores);

        } catch (Exception ex) {
            ex.printStackTrace();
            pantallaErrores(new Exception("No se han podido mostrar los repartidores"));
        }
    }

    @FXML
    void buscarRepartidor(MouseEvent event) {

    }

    @FXML
    void selecRepartidor(MouseEvent event) {
        this.repartidorSeleccionado = tvRepartidores.getSelectionModel().getSelectedItem();

        if (repartidorSeleccionado != null) {
            //Generar datos para el reparto y facturacion
            try {
                //Obtenemos las rutas activas del repartidor seleccionado.
                List<BatoiLogicRuta> rutasActivasDelRepartidor = rutaDAO.findByRepartidor(repartidorSeleccionado);
                BatoiLogicCamion camionDelRepartidor = repartidorDAO.findByPK(repartidorSeleccionado.getId()).getBatoiLogicCamion();

                //Comprobamos si los nuevos pedidos no superan la capacidad de almacenamiento del camión.
                if ((rutasActivasDelRepartidor.size() + pedidosSeleccionados.size()) <= camionDelRepartidor.getCapacidad()) {
                    BatoiLogicRuta nuevaRuta = generarRuta();
                    generarAlbaranes(nuevaRuta);
                    volverPedidos(event);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Albaranes generados");
                    alert.setHeaderText(null);
                    alert.setContentText("Los albaranes han sido generados y asignados al repartidor.");

                    alert.showAndWait();
                } else {
                    pantallaErrores(new Exception("El camión de este repartidor está lleno y no podrá transportar los pedidos seleccionados"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            pantallaErrores(new Exception("No se ha seleccionado ningun repartidor"));
        }
    }

    public void generarAlbaranes(BatoiLogicRuta nuevaRuta) throws Exception {
        ArrayList<BatoiLogicAlbaran> nuevosAlbaranes = new ArrayList<>();
        BatoiLogicProducto productoConStockActualizado;

        for (BatoiLogicPedido pedido : pedidosSeleccionados) {

            List<BatoiLogicLinpedido> lineasDelPedido = (List<BatoiLogicLinpedido>) linPedidoDAO.findByPedido(pedido);
            for (int i = 0; i < lineasDelPedido.size(); i++) {
                // Actualización de stock.
                productoConStockActualizado = lineasDelPedido.get(i).getBatoiLogicPedidoByProducto();
                productoConStockActualizado.setStock(productosDAO.findByPK(productoConStockActualizado.getId()).getStock() - ((BatoiLogicLinpedido) linPedidoDAO.findByPedido(pedido).get(0)).getCantidad());
                productosDAO.update(productoConStockActualizado);
            }

            pedidoDAO.update(pedido);
            nuevosAlbaranes.add(new BatoiLogicAlbaran(pedido, nuevaRuta));
        }
        AlbaranDAO albDAO = new AlbaranDAO();
        for (BatoiLogicAlbaran albaran : nuevosAlbaranes) {
            albDAO.insert(albaran);
            crearFacturas(albaran);
        }
    }

    public BatoiLogicRuta generarRuta() throws Exception {
        BatoiLogicRuta ruta = new BatoiLogicRuta(repartidorSeleccionado);
        RutaDAO rDao = new RutaDAO();
        rDao.insert(ruta);

        return ruta;
    }

    private void crearFacturas(BatoiLogicAlbaran albaran) throws Exception {
        FacturaDAO factDao = new FacturaDAO();
        BatoiLogicFactura nuevaFactura = new BatoiLogicFactura(0, albaran, albaran.getBatoiLogicPedido().getBatoiLogicDireccion());
        factDao.insert(nuevaFactura);
    }

    /*
    Proveedores
     */
    @FXML
    private TableColumn<BatoiLogicProveedor, String> provClCIF;
    @FXML
    private TableColumn<BatoiLogicProveedor, String> provClNombre;
    @FXML
    private TableColumn<BatoiLogicProveedor, Double> provClEmail;
    @FXML
    private TableColumn<BatoiLogicProveedor, Integer> provClTelefono;
    @FXML
    private TableView<BatoiLogicProveedor> tvProveedores;

    private void initializeProveedores() {
        try {
            proveedores = proveedorDAO.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        provClCIF.setCellValueFactory(new PropertyValueFactory<>("cif"));
        provClNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        provClEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        provClTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));

        tvProveedores.getItems().setAll(proveedores);
    }
    /*
    Inventario
     */
    @FXML
    private TableColumn<BatoiLogicProducto, String> invColNombre;
    @FXML
    private TableColumn<BatoiLogicProducto, String> invColDesc;
    @FXML
    private TableColumn<BatoiLogicProducto, Double> invColPrecio;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> invColStock;
    @FXML
    private TableView<BatoiLogicProducto> tvInventario;

    private void initializeInventario() {
        try {
            productosDisponibles = productosDAO.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        invColNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        invColDesc.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        invColPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));
        invColStock.setCellValueFactory(new PropertyValueFactory<>("stock"));

        tvInventario.getItems().setAll(productosDisponibles);
    }

    /**
     * REALIZAR PEDIDO.
     */
    @FXML
    private TableView<BatoiLogicProducto> rpTvArticulosDisponibles;
    @FXML
    private TableColumn<BatoiLogicProducto, Integer> rpClReferencia;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClNombre;
    @FXML
    private TableColumn<BatoiLogicProducto, String> rpClDescripcion;
    @FXML
    private Button rpBtRealizarPedido;

    private void initializeRealizarPedido() {

        /*try {
            productosDisponibles = productosDAO.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        // Configuramos las columnas de la rpTvArticulosDisponibles.
        rpClReferencia.setCellValueFactory(new PropertyValueFactory<>("id"));
        rpClNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        rpClDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));

        rpTvArticulosDisponibles.getItems().setAll(productosDisponibles);

        // Configuramos las columnas de la rpTvCarrito.
    }

    /**
     * Funcion que detecta si un registro ha sido seleccionado.
     */
    public void registroSeleccionadoEnArticulosDisp() {

        if (rpTvArticulosDisponibles.getSelectionModel().getSelectedItem() != null) {
            rpBtRealizarPedido.setDisable(false);
        } else {
            rpBtRealizarPedido.setDisable(true);
        }

    }

    @FXML
    void realizarPedido(ActionEvent event) {
        try {
            BatoiLogicProducto productoSeleccionado = new BatoiLogicProducto(rpTvArticulosDisponibles.getSelectionModel().getSelectedItem());

            TextInputDialog ventanaCantidad = new TextInputDialog("1");
            ventanaCantidad.setTitle("Confirmar cantidad");
            ventanaCantidad.setHeaderText("Por favor, indique la cantidad a comprar");
            ventanaCantidad.setContentText("Introduzca un valor numérico entero:");

            Optional<String> cantidadAComprar = ventanaCantidad.showAndWait();
            if (cantidadAComprar.isPresent()) {

                if (Integer.parseInt(cantidadAComprar.get()) > 0) {

                    ProductoProveedorDAO productoProveedorDAO = new ProductoProveedorDAO();
                    List<BatoiLogicProductoproveedor> productoproveedors = productoProveedorDAO.findByProducto(productoSeleccionado.getId());

                    List<String> opciones = new ArrayList<>();

                    proveedores = proveedorDAO.findAll();

                    for (BatoiLogicProductoproveedor productoproveedor : productoproveedors) {
                        for (BatoiLogicProveedor proveedor : proveedores) {
                            if (proveedor.getId() == productoproveedor.getBatoiLogicProveedor().getId()) {
                                opciones.add(proveedor.getId() + " " + proveedor.getNombre() + " - " + ((double) Math.round((productoproveedor.getPrecio() * Integer.parseInt(cantidadAComprar.get())) * 100d) / 100d) + " €");
                            }
                        }
                    }

                    ChoiceDialog<String> dialog = new ChoiceDialog<>("", opciones);
                    dialog.setTitle("Ventana de proveedor");
                    dialog.setHeaderText("Seleccione proveedor");
                    dialog.setContentText("Seleccione el proveedor al que solicitará la mercancía:");

                    // Traditional way to get the response value.
                    Optional<String> proveedorSeleccionado = dialog.showAndWait();

                    if (proveedorSeleccionado.isPresent()) {
                        if (!proveedorSeleccionado.get().equals("")) {

                            BatoiLogicPedidoprov pedidoprov = new BatoiLogicPedidoprov();
                            pedidoprov.setBatoiLogicProducto(productosDAO.findByPK(productoSeleccionado.getId()));
                            pedidoprov.setBatoiLogicProveedor(proveedorDAO.findByPK(Integer.parseInt(proveedorSeleccionado.get().split(" ")[0])));
                            pedidoprov.setCantidad(Integer.parseInt(cantidadAComprar.get()));
                            pedidoprov.setFecha(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));

                            //Insertamos pedido.
                            PedidoProvDAO pedidoProvDAO = new PedidoProvDAO();
                            pedidoProvDAO.insert(pedidoprov);

                            //Actualizamos stock.
                            BatoiLogicProducto productoConStockActualizado = productosDAO.findByPK(productoSeleccionado.getId());
                            productoConStockActualizado.setStock(productoConStockActualizado.getStock() + pedidoprov.getCantidad());
                            productosDAO.update(productoConStockActualizado);

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Pedido realizado");
                            alert.setHeaderText(null);
                            alert.setContentText("El pedido ha sido realizado correctamente al proveedor.");

                            alert.showAndWait();

                            volverPedidos(event);

                        } else {
                            pantallaErrores(new Exception("Seleccione un proveedor para completar la compra."));
                        }
                    }

                } else {
                    pantallaErrores(new Exception("No se puede introducir 0 o menos"));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Función que mostrará por pantalla los mensajes de error.
     *
     * @param ex
     */
    private void pantallaErrores(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Se ha producido un error");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("El trazado ha devuelto el siguiente error:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

}
